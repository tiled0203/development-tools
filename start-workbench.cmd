@echo off
REM Portable Startscript for MySQL Workbench
SET USERPROFILE=%CD%
IF NOT EXIST "%CD%\AppData\Roaming\MySQL\Workbench" MD "%CD%\AppData\Roaming\MySQL\Workbench"
START MySql-WorkBench\WorkBench\MySQLWorkbench.exe
REM Close this screen
EXIT